APP
.factory('loginSvc', ['$http', function($http) {
    var baseUrl = 'http://dev.letsride.ca/pg/user/';

    function get(qString, cb) {
        $http.get(baseUrl + qString).success(function(data) {
            typeof cb == 'function' && cb(data);
        });
    };

    function post(url, params, cb) {
        $http.post(baseUrl + url, params).success(function(data) {
            typeof cb == 'function' && cb(data);
        });
    }

    return {
        login: function(user, passwd, cb) { 
            get('login?login=' + user + '&passwd=' + passwd, cb); 
        },
        isLogged:   function(cb)            { get('isLogged', cb); },
        logout:     function(cb)            { get('logout', cb); },
        register:   function(params, cb)    { post('register', params, cb); }
    }
}])
.directive('header', ['loginSvc', function(loginSvc) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: 'html/header.html',
        link: function($scope, el, attrs) {

            $scope.login    = 'Jo';
            $scope.passwd   = 'secret';

            $scope.doLogin = function() {
                if ($scope.logged)
                    loginSvc.logout(function(retData) {
                        $scope.logged = retData.success;
                    });
                else 
                    loginSvc.login($scope.login, $scope.passwd, function(retData) {
                        $scope.logged = retData.success;
                    });
            }
            
            // upon loading we want to know if we have a valid session
            loginSvc.isLogged(function(ret) { $scope.logged = ret.success; });
        }
    }
}])
