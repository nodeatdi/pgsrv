APP
.factory('userFormSvc', ['$http', function($http) {
    var baseUrl = 'http://dev.letsride.ca/pg/user/';

    function get(qString, cb) {
        $http.get(baseUrl + qString).success(function(data) {
            typeof cb == 'function' && cb(data);
        });
    };

    function post(url, params, cb) {
        $http.post(baseUrl + url, params).success(function(data) {
            typeof cb == 'function' && cb(data);
        });
    }

    return {
        getUsers: function(cb)          { get('getUsers', cb); },
        delUser: function(guid, cb)     { get('delUser?guid=' + guid, cb); },
        addUser: function(params, cb)   { post('addUser', params, cb); },
        updUser: function(params, cb)   { post('updUser', params, cb); },
        setPw:   function(params, cb)   { post('setPw',   params, cb); }
    }
}])
.directive('userForm', ['userFormSvc', function(userFormSvc) {
    var scope;
    function setData(retData) { 
        if (retData.success) {
            for (var i=0; i<retData.data.length; i++) retData.data[i].passwd = '';
            scope.users = retData.data; 
        } else {
            scope.users = null;
        }
    }

    return {
        restrict: 'A',
        replace: true,
        templateUrl: 'html/users.html',
        link: function($scope, el, attrs) {
            scope = $scope;
            $scope.users = [];

            $scope.getUsers = function() {
                userFormSvc.getUsers(setData);
            }
            $scope.getUsers();

            $scope.delUser = function(idx) {
                if ($scope.users[idx].guid == '')
                    $scope.users.shift();
                else
                    if (confirm('Delete user ' + $scope.users[idx].first + ' ' + $scope.users[idx].last))
                        userFormSvc.delUser($scope.users[idx].guid, setData);
            }

            $scope.addUser = function() {
                userFormSvc.addUser($scope.users[0], setData);
            }

            $scope.updUser = function(idx) {
                userFormSvc.updUser($scope.users[idx], setData);
            }

            $scope.newUser = function() {
                $scope.users.length && $scope.users[0].guid == '' || 
                    $scope.users.unshift( {guid: '', email: '', first: '', last: '', perms:''});
            }
            $scope.setPw = function(idx) {
                console.log( $scope.users );
                userFormSvc.setPw($scope.users[idx], setData);
            }
        }
    }
}])
