// Model
var pg = require('pg');
//                        user:password             dbName
var connStr  = 'postgres://ride:donkeyRide@localhost/ride';
const QUOT   = 0;
const NOQUOT = 1;

function qFormat(str, values) {
    for (var i=0; i<values.length; i++)
        str = str.replace("{" + i + "}", values[i]);

    return str;
}

function execQuery(sql, cb) {
    console.log( sql );
    new pg.Client(connStr).connect(function(e,conn) {
        e   ? cb(e)
            : conn.query(sql, function(err, res){
                err && (err.query = sql);
                err && cb(err);
                err || cb(typeof res.rows == 'undefined' ? res : res.rows);
                conn.end();
            });
    });
}

function crypt(arg) {
    return require('crypto').createHash('md5').update(arg).digest('hex');
}

function getUsers(cb) {
    execQuery(sql, function() { execQuery("SELECT * FROM users ORDER BY last,first", cb); });
}

module.exports = {
    login:      function(q, cb) { cb(q); },
    getUsers:   function(q, cb) { getUsers(cb); },
    getUser:    function(q, cb) { 
        execQuery(qFormat("SELECT * FROM users WHERE guid='{0}'", [guid], cb)); 
    },
    authUser: function(q, cb)    { 
        execQuery(qFormat("SELECT * FROM users WHERE login='{0}' AND passwd='{1}'", [q.login, crypt(q.passwd)]), cb);
    },
    delUser: function(guid, cb)  { 
        execQuery(qFormat("DELETE FROM users WHERE guid='{0}'", [guid]), function() {
            getUsers(cb);
        }); 
    },
    addUser: function(q, cb)    { 
        var quid    = 'md5(random()::text || clock_timestamp()::text)::uuid';
        var sql     = qFormat("INSERT INTO users (guid, first, last, email, perms, passwd, login) VALUES " +
                        "({0},  '{1}',   '{2}',   '{3}',    {4},     '{5}',           '{6}')",   
                         [quid,  q.first, q.last,  q.email, q.perms,  crypt(q.passwd), q.login ]);

        execQuery(sql, function() { getUsers(cb); });
    },
    updUser: function(q, cb)  { 
        var sql = qFormat("UPDATE users SET " +
                    "login='{0}', first='{1}', last='{2}', email='{3}', perms={4}          WHERE guid='{5}'", 
                    [q.login,     q.first,     q.last,     q.email,     parseInt(q.perms),       q.guid]);

        execQuery(sql, function() { getUsers(cb); });
    },
    setPw: function(q, cb) {
        var sql = qFormat("UPDATE users SET  passwd='{0}'       WHERE guid='{1}'", 
                                                    [crypt(q.passwd), q.guid]);
        execQuery(sql, function() { getUsers(cb); });
    }
}
