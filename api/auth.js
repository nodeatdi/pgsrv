var DS = require('nedb');
var nedb = new DS({filename: '/data/ne/auth.db', autoload: true});
nedb.persistence.setAutocompactionInterval(10000);

var userMod = require('./models/user.js'),
    url     = require('url');

function expire(inPast) {
    var sessTimeout = typeof inPast != 'undefined' && inPast ? -11800 : 1800; // seconds
    return new Date(new Date().getTime()+(sessTimeout*1000)).toUTCString();
}

function guid() {
  	return 		Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)
		+ 		Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)
		+ '-' + Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)
		+ '-' + Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)
		+ '-' + Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)
		+ '-' + Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)
		+ 		Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)
		+ 		Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
}

function parseCookies (request) {
    var list = {}, rc = request.headers.cookie;

    rc && rc.split(';').forEach(function( cookie ) {
        var parts = cookie.split('=');
        list[parts.shift().trim()] = decodeURI(parts.join('='));
    });
    return list;
}

function respObj(newLogin, creds, logged) {
    return {"newLogin": newLogin, "creds": creds, "logged": logged};
}

function crypt(arg) {
    return require('crypto').createHash('md5').update(arg).digest('hex');
}

var auth = module.exports = {
    checkLogin: function(request, response, cb) {
        var path = require('url').parse(request.url, true).pathname;

        var brCookie = parseCookies(request).sessId; 
        if (path.indexOf('logout') == -1) { 
            if (typeof brCookie == 'undefined' || !brCookie) { // no session, this is login
                var q = url.parse(request.url, true).query;
                if (typeof q.login != 'undefined' && q.login && typeof q.passwd != 'undefined' && q.passwd) {
                    userMod.authUser(q, function(creds) {
                        if (creds.length) {
                            auth.putCookie(request, response, q.login);
                            cb(respObj(true, creds[0], true));
                        } else { 
                            cb(respObj(true, false, false));
                        }
                    });
                } else {
                    cb(respObj(true, false, false)); // bad login - no credentials provied
                }
            } else {
                auth.checkCookie(brCookie, function(retObj) {
                    if (retObj) {
                        nedb.update(
                            {cookie: brCookie}, {$set: {expires: expire()}},{multi: true, upsert:false}
                        );
                        cb(respObj(false, retObj, retObj));
                    } else { // clear stale browser cookie
                        response.writeHead(200, auth.cookieHeader(''));
                        cb(respObj(false, false, false));
                    }
                });
            }
        } else { // logout
            response.writeHead(200, auth.cookieHeader(''));
            nedb.remove({ip:  request.headers['x-real-ip'].trim(), cookie: brCookie}, function() {
                cb(respObj(true, false, false));
            });
        }
    },
	checkCookie: function(brCookie, cb) {
		if ( typeof brCookie != 'undefined' ) {
            nedb.find({cookie: brCookie}, function(e1, res) {
                var expired =   typeof res[0] == 'undefined' || res[0].cookie == 'expired' ||
                                new Date(res[0].expires).getTime() - new Date().getTime() < 0;

                cb(expired ? false : res.length);
			});
         } else {
            cb(false);
         }
	}, 
    putCookie: function(req, resp, login) {
        var newCookie = guid();
        resp.writeHead(200, auth.cookieHeader(newCookie));

        nedb.remove({login: login}, function() {
            nedb.insert({ip:  req.headers['x-real-ip'], cookie: newCookie, login: login, expires: expire() }, function() {});
        });
	}, 
	cookieHeader: function(cookie) {
		return {'Set-Cookie' : 'sessId=' + cookie + ';httpOnly; path=/; expires=' + expire(cookie=='')};
	}
}
