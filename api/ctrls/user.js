// Controller

var mod = require('../models/user.js');

function validate(q, fields, valid) {
    return true;
}

module.exports = {
    isLogged: function(q, cb) { cb({"success": true}); }, // if called from router then we must be authorized
    getUsers: function(q, cb) { 
        mod.getUsers(q, function(retO) { cb(retO); }
    )},
    setPw: function(q, cb) { 
        if (typeof q.guid == 'undefined' || typeof q.passwd == 'undefined')
            cb({"success": false, "msg": "User guid or password is missing"});
        else 
            mod.setPw(q, function(retO) { cb(retO); }
    )},
    getUser: function(q, cb) { 
        if (typeof q.guid == 'undefined')
            cb({"success": false, "msg": "User guid is missing"});
        else 
            mod.getUser(q.guid, function(retO) { cb(retO); }); 
    },
    delUser: function(q, cb)  { 
        if (typeof q.guid == 'undefined')
            cb({"success": false, "msg": "User guid is missing"});
        else 
            mod.delUser(q.guid, function(retO) { cb(retO); }); 
    },
    addUser: function(q, cb)    { 
        if (validate(q, [], [])) 
            mod.addUser(q,  function(retO) { cb(retO); }); 
        else
            cb({"success": false, "msg": "Parameter error"});
    },
    updUser: function(q, cb)  { 
        if (validate(q, [], [])) 
            mod.updUser(q,  function(retO) { cb(retO); }); 
        else
            cb({"success": false, "msg": "Parameter error"});
    }
}
