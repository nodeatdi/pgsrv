require('fs').writeFile('./bin/killme', "#!/bin/bash\nkill " + process.pid.toString());
require('fs').writeFile('./bin/restart', "#!/bin/bash\nkill " + process.pid.toString() + "\nnohup nodejs `pwd`/srv.js 2>&1 >log/srv.log &\n");

var route   = require('./route');
//console.log( require('crypto').createHash('md5').update('secret').digest('hex') );

require('http').createServer(function (request, response) {
    require('./auth').checkLogin(request, response, function(creds) {
        if (!creds.newLogin && creds.logged) {
            if (request.method == 'POST') {
                var body = '';
                request.on('data',function(data) { body += data; });
                request.on('end', function() { route.go(request, response, JSON.parse(body)); });
            } else {
                route.go(request, response, require('url').parse(request.url, true).query);
            }
        } else { // after login or logout
            if (creds.logged) {
                var c = creds.creds;
                response.end(JSON.stringify({"success": true, "mgs": {
                    "first":    c.first,
                    "last":     c.last,
                    "login":    c.login,
                    "perms":    c.perms
                }}));
            } else { // bad login or good logout
                response.end('{"success": false, "mgs": "Not authorized"}');
            }
        }
    });
})
.listen(7571);

console.log('Sever running');
