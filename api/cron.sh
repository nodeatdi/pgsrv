PWD=/home/jarek/www/pgtest
SRVD=${PWD}/srv
SRV=${SRVD}/srv.js
echo $PWD
if [ `ps -ef | grep $SRV | grep node | grep -v grep | wc | cut -c7-7` -eq 0 ]; then
    echo Client API crashed at: `date` >> /tmp/cron.log
    cat ${SRVD}/log/srv.log> /tmp/cron.log
    echo ${SRVD}
    ${SRVD}/bin/restart
    sleep 1
    if [ `ps -ef | grep ${SRV} | grep node | grep -v grep | wc | cut -c7-7` -eq 0 ]; then
        #echo Client API down | mail jarek707@gmail.com -s 'Client API down. Restart failed'
        echo Restart Failed, will try again >> /tmp/cron.log
    else
        #echo Client API down | mail jarek707@gmail.com -s 'Client API down. Restart successful'
        echo Restart Successful >> /tmp/cron.log
    fi
fi
