//ROUTER
function getCtrl(name) {
    if (require('fs').existsSync('./ctrls/' + name + '.js'))
        return require('./ctrls/' + name + '.js');
    else 
        return false;
}

function formatResp(data, success, msg) {
    return JSON.stringify({"success": success, "data": data, "msg": msg});
}

module.exports = {
    go: function(req, res, requestData) {
        var path = require('url').parse(req.url, true).pathname.split('/');
        path[0] == '' && path.shift();
        ['pg'].indexOf(path[0]) > -1 && path.shift(); // put all nginx directory redirects in the array
        var ctrl = getCtrl(path.shift());
        
        if (ctrl) {
            var action = path.shift();
            if (typeof ctrl[action] != 'undefined') 
                ctrl[action](requestData, function(retO) {
                    res.end(formatResp(retO, true, ''));
                });
            else
                res.end(formatResp('noAction', false, "Router: action not defined."));
        } else {
            res.end(formatResp('noController', false, "Router: controller not defined."));
        }
    }
}
